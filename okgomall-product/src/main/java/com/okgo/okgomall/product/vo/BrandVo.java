package com.okgo.okgomall.product.vo;

import lombok.Data;

@Data
public class BrandVo {

    /**
     * "brandId": 0,
     * "brandName": "string",
     */
    private Long brandId;
    private String  brandName;
}
