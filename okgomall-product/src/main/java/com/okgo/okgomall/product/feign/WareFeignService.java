package com.okgo.okgomall.product.feign;

import com.okgo.common.to.SkuHasStockVo;
import com.okgo.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author Shawn
 * @date 2020/9/16 23:09
 * @title Function
 */
@FeignClient("okgomall-ware")
public interface WareFeignService {
    @PostMapping("/ware/waresku/hasstock")
    R getSkuHasStock(@RequestBody List<Long> skuIds);
}
