package com.okgo.okgomall.product.vo;

import lombok.Data;

/**
 * @author Shawn
 * @date 2020/9/26 18:09
 * @title Function
 */

@Data
public class AttrValueWithSkuIdVo {

    private String attrValue;

    private String skuIds;

}
