package com.okgo.okgomall.product.fallback;

import com.okgo.common.exception.BizCodeEnume;
import com.okgo.common.utils.R;
import com.okgo.okgomall.product.feign.SeckillFeignService;
import org.springframework.stereotype.Component;

/**
 * @author Shawn
 * @date 2020/10/13 7:57
 * @title Function
 */

@Component
public class SeckillFeignServiceFallBack implements SeckillFeignService {
    @Override
    public R getSkuSeckilInfo(Long skuId) {
        return R.error(BizCodeEnume.TO_MANY_REQUEST.getCode(),BizCodeEnume.TO_MANY_REQUEST.getMsg());
    }
}
