package com.okgo.okgomall.product.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * @author Shawn
 * @date 2020/9/21 21:00
 * @title Function
 */
@Configuration
public class MyRedissonConfig {

    /**
     * 所有对Redis的操作都是通过 RedissonClient 对象
     * @return
     * @throws IOException
     */
    @Bean(destroyMethod="shutdown")
    public RedissonClient redissonClient() throws IOException {
        // 1. 创建配置
        Config config = new Config();
        config.useSingleServer().setAddress("redis://192.168.107.130:6379");
        // 2. 根据Config创建出 RedissonClient 实例
        return Redisson.create(config);
    }
}
