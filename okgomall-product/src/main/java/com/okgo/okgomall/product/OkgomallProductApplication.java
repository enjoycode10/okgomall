package com.okgo.okgomall.product;

import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.annotation.MapperScans;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@EnableRedisHttpSession
@EnableCaching
@EnableFeignClients(basePackages = "com.okgo.okgomall.product.feign")
@MapperScan("com.okgo.okgomall.product.dao")
@SpringBootApplication
@EnableDiscoveryClient
public class OkgomallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(OkgomallProductApplication.class, args);
    }

}
