package com.okgo.okgomall.product.vo;

import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @author Shawn
 * @date 2020/9/26 18:09
 * @title Function
 */

@Data
@ToString
public class SpuItemAttrGroupVo {

    private String groupName;

    private List<Attr> attrs;

}
