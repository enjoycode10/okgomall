package com.okgo.okgomall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.okgo.common.utils.PageUtils;
import com.okgo.okgomall.product.entity.CommentReplayEntity;

import java.util.Map;

/**
 * 商品评价回复关系
 *
 * @author Shawn
 * @email enjoycode10@163.com
 * @date 2019-10-01 21:08:48
 */
public interface CommentReplayService extends IService<CommentReplayEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

