package com.okgo.okgomall.product.feign;

import com.okgo.common.to.es.SkuESModel;
import com.okgo.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author Shawn
 * @date 2020/9/17 9:16
 * @title Function
 */
@FeignClient("okgomall-search")
public interface SearchFeignService {
    // 上架商品
    @PostMapping("/search/save/product")
    R productStatusUp(@RequestBody List<SkuESModel> skuESModels);
}
