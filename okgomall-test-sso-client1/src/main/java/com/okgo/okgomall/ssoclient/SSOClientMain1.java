package com.okgo.okgomall.ssoclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SSOClientMain1 {

    public static void main(String[] args) {
        SpringApplication.run(SSOClientMain1.class, args);
    }

}
