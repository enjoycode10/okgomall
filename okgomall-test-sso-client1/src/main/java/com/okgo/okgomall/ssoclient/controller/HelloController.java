package com.okgo.okgomall.ssoclient.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Shawn
 * @date 2020/9/28 22:05
 * @title 单点登录客户端
 */
@Controller
public class HelloController {

    @Value("sso.server.url")
    String ssoServerUrl;

    @GetMapping("emps")
    public String emps(Model model) {
        List<String> emps = Arrays.asList("张三", "李四", "王五");
        model.addAttribute("emps", emps);
        return "employees";
    }

}
