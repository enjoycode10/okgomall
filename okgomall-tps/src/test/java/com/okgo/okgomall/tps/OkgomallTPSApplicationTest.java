package com.okgo.okgomall.tps;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSClientBuilder;
import com.okgo.okgomall.tps.component.SmsComponent;
import com.okgo.okgomall.tps.utils.HttpUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Shawn
 * @date 2020/8/15 18:17
 * @title Function
 */
@SpringBootTest
public class OkgomallTPSApplicationTest {
    @Test
    void contextLoads(){

    }

    @Autowired
    SmsComponent smsComponent;

    @Test
    public void sendSms(){
        smsComponent.sendSms("15196699086", "123456");
        System.out.println("发送短信结束");
    }

    @Autowired
    OSSClient ossClient;
    @Test
    public void testUpload() throws FileNotFoundException {
        // Endpoint以杭州为例，其它Region请按实际情况填写。
        String endpoint = "oss-cn-shanghai.aliyuncs.com";
        // 云账号AccessKey有所有API访问权限，建议遵循阿里云安全最佳实践，创建并使用RAM子账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建。
        String accessKeyId = "LTAI4GFa5R8CFwrH5RnnceDd";
        String accessKeySecret = "0haLUOCPl9dmqcrgBv8bfGvyCq0uVC";

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        // 上传文件流。
        InputStream inputStream = new FileInputStream("C:\\Users\\Shawn\\Pictures\\Saved Pictures\\微信图片_20200725100215.jpg");
        ossClient.putObject("okgomall", "dogHaha.jpg", inputStream);

        // 关闭OSSClient。
        ossClient.shutdown();

        System.out.println("上传完成");
    }

}
