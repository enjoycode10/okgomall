package com.okgo.okgomall.tps.controller;

import com.okgo.common.utils.R;
import com.okgo.okgomall.tps.component.SmsComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Shawn
 * @date 2020/9/27 9:39
 * @title Function
 */
@RestController
@RequestMapping("sms")
public class SmsController {

    @Autowired
    SmsComponent smsComponent;

    /**
     * 提供给别的服务进行调用
     * @param phone
     * @param code
     * @return
     */
    @GetMapping("/sendcode")
    public R sendCode(@RequestParam("phone") String phone, @RequestParam("code") String code){
        smsComponent.sendSms(phone, code);
        return R.ok();
    }
}
