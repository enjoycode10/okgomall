package com.okgo.okgomall.tps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Shawn
 * @date 2020/8/15 18:17
 * @title Function
 */
@SpringBootApplication
@EnableDiscoveryClient
public class OkgomallTPSApplication {
    public static void main(String[] args) {
        SpringApplication.run(OkgomallTPSApplication.class, args);
    }
}
