package com.okgo.okgomall.seckill.feign;

import com.okgo.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Shawn
 * @date 2020/10/11 21:40
 * @title Function
 */

@FeignClient("okgomall-coupon")
public interface CouponFeignService {

    /**
     * 查询最近三天需要参加秒杀商品的信息
     * @return
     */
    @GetMapping(value = "/coupon/seckillsession/latest3DaySession")
    R getLates3DaySession();

}
