package com.okgo.okgomall.seckill.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author Shawn
 * @date 2020/10/11 21:40
 * @title Function
 */
@EnableAsync
@EnableScheduling
@Configuration
public class ScheduledConfig {

}
