package com.okgo.okgomall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.okgo.common.utils.PageUtils;
import com.okgo.okgomall.coupon.entity.CouponHistoryEntity;

import java.util.Map;

/**
 * 优惠券领取历史记录
 *
 * @author Shawn
 * @email enjoycode10@163.com
 * @date 2020-07-17 08:41:18
 */
public interface CouponHistoryService extends IService<CouponHistoryEntity> {

    com.okgo.common.utils.PageUtils queryPage(Map<String, Object> params);
}

