package com.okgo.okgomall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.okgo.common.utils.PageUtils;
import com.okgo.okgomall.coupon.entity.MemberPriceEntity;

import java.util.Map;

/**
 * 商品会员价格
 *
 * @author Shawn
 * @email enjoycode10@163.com
 * @date 2020-07-17 08:41:18
 */
public interface MemberPriceService extends IService<MemberPriceEntity> {

    com.okgo.common.utils.PageUtils queryPage(Map<String, Object> params);
}

