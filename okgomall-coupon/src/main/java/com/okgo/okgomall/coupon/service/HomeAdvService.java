package com.okgo.okgomall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.okgo.common.utils.PageUtils;
import com.okgo.okgomall.coupon.entity.HomeAdvEntity;

import java.util.Map;

/**
 * 首页轮播广告
 *
 * @author Shawn
 * @email enjoycode10@163.com
 * @date 2020-07-17 08:41:18
 */
public interface HomeAdvService extends IService<HomeAdvEntity> {

    com.okgo.common.utils.PageUtils queryPage(Map<String, Object> params);
}

