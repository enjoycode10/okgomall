package com.okgo.okgomall.coupon.dao;

import com.okgo.okgomall.coupon.entity.SeckillSkuNoticeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀商品通知订阅
 * 
 * @author Shawn
 * @email enjoycode10@163.com
 * @date 2020-07-17 08:41:17
 */
@Mapper
public interface SeckillSkuNoticeDao extends BaseMapper<SeckillSkuNoticeEntity> {
	
}
