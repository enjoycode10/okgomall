package com.okgo.okgomall.coupon.dao;

import com.okgo.okgomall.coupon.entity.SeckillSessionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动场次
 * 
 * @author Shawn
 * @email enjoycode10@163.com
 * @date 2020-07-17 08:41:17
 */
@Mapper
public interface SeckillSessionDao extends BaseMapper<SeckillSessionEntity> {
	
}
