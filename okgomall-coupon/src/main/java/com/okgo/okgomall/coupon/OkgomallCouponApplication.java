package com.okgo.okgomall.coupon;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@MapperScan("com.okgo.okgomall.coupon.dao")
@SpringBootApplication
public class OkgomallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(OkgomallCouponApplication.class, args);
    }

}
