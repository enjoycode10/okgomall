package com.okgo.okgomall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.okgo.common.utils.PageUtils;
import com.okgo.okgomall.coupon.entity.CouponSpuRelationEntity;

import java.util.Map;

/**
 * 优惠券与产品关联
 *
 * @author Shawn
 * @email enjoycode10@163.com
 * @date 2020-07-17 08:41:18
 */
public interface CouponSpuRelationService extends IService<CouponSpuRelationEntity> {

    com.okgo.common.utils.PageUtils queryPage(Map<String, Object> params);
}

