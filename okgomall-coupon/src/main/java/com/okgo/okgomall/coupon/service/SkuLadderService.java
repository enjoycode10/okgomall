package com.okgo.okgomall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.okgo.common.utils.PageUtils;
import com.okgo.okgomall.coupon.entity.SkuLadderEntity;

import java.util.Map;

/**
 * 商品阶梯价格
 *
 * @author Shawn
 * @email enjoycode10@163.com
 * @date 2020-07-17 08:41:17
 */
public interface SkuLadderService extends IService<SkuLadderEntity> {

    com.okgo.common.utils.PageUtils queryPage(Map<String, Object> params);
}

