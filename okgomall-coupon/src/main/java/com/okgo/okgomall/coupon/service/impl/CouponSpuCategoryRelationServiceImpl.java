package com.okgo.okgomall.coupon.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.okgo.common.utils.PageUtils;
import com.okgo.common.utils.Query;

import com.okgo.okgomall.coupon.dao.CouponSpuCategoryRelationDao;
import com.okgo.okgomall.coupon.entity.CouponSpuCategoryRelationEntity;
import com.okgo.okgomall.coupon.service.CouponSpuCategoryRelationService;


@Service("couponSpuCategoryRelationService")
public class CouponSpuCategoryRelationServiceImpl extends ServiceImpl<CouponSpuCategoryRelationDao, CouponSpuCategoryRelationEntity> implements CouponSpuCategoryRelationService {

    @Override
    public com.okgo.common.utils.PageUtils queryPage(Map<String, Object> params) {
        IPage<CouponSpuCategoryRelationEntity> page = this.page(
                new com.okgo.common.utils.Query<CouponSpuCategoryRelationEntity>().getPage(params),
                new QueryWrapper<CouponSpuCategoryRelationEntity>()
        );

        return new com.okgo.common.utils.PageUtils(page);
    }

}