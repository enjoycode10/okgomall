package com.okgo.okgomall.auth.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author Shawn
 * @date 2020/9/26 23:30
 * @title 定制SpringMVC的功能。基于java8的接口新特性，WebMvcConfigurer已有默认实现
 */
@Configuration
public class OkgomallWebConfig implements WebMvcConfigurer {


    /**
     * addViewController(String urlPath): 对应 controller 中的 @GetMapping("/login.html") 中的值
     * setViewName(String viewName): 对应 controller 中的 return "login" 返回的值
     *
     * @param registry
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login.html").setViewName("login");
        registry.addViewController("/register.html").setViewName("register");
    }
}
