package com.okgo.okgomall.auth.vo;

import lombok.Data;

/**
 * @author Shawn
 * @date 2020/9/27 13:05
 * @title Function
 */

@Data
public class UserLoginVo {

    private String loginacct;

    private String password;
}
