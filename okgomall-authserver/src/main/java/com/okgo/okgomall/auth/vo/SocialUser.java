package com.okgo.okgomall.auth.vo;

import lombok.Data;

/**
 * @author Shawn
 * @date 2020/9/27 13:06
 * @title Function
 */

@Data
public class SocialUser {

    private String access_token;

    private String remind_in;

    private long expires_in;

    private String uid;

    private String isRealName;

}
