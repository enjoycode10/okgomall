package com.okgo.okgomall.ssoclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SSOClientMain2 {

    public static void main(String[] args) {
        SpringApplication.run(SSOClientMain2.class, args);
    }

}
