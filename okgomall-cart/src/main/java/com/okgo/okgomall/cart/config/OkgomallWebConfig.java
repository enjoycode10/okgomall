package com.okgo.okgomall.cart.config;

import com.okgo.okgomall.cart.interceptor.CartInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author Shawn
 * @date 2020/9/29 21:06
 * @title Function
 */
@Configuration
public class OkgomallWebConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new CartInterceptor()).addPathPatterns("/**"); // 拦截所有请求
    }
}
