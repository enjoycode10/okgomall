package com.okgo.okgomall.cart.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Shawn
 * @date 2020/9/29 21:05
 * @title Function
 */
@ConfigurationProperties(prefix = "okgomall.thread")
@Component
@Data
public class ThreadPoolConfigProperties {
    private Integer coreSize;
    private Integer maxSize;
    private Integer keepAliveTime;
}
