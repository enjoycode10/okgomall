package com.okgo.okgomall.cart.to;

import lombok.Data;

/**
 * @author Shawn
 * @date 2020/9/29 20:17
 * @title 在执行目标方法之前，判断用户的登录状态.并封装传递给controller目标请求
 */
@Data
public class UserInfoTo {

    /**
     * 用户登录的情况下有userId
     */
    private Long userId;

    /**
     * 用户没登录的情况下有临时键
     */
    private String userKey;

    /**
     * 是否临时用户
     */
    private Boolean tempUser = false;

}
