package com.okgo.okgomall.ssoserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;


@Controller
public class LoginController {

    @Autowired
    StringRedisTemplate redisTemplate;

    /**
     * 根据token 获取用户的session数据
     * @param token
     * @return
     */
    @ResponseBody
    @GetMapping("/userInfo")
    public String userinfo(@RequestParam(value = "token") String token) {

        return redisTemplate.opsForValue().get(token);

    }

    /**
     * 接收客户端的重定向登录请求、
     * 并将来源url存入登录表单隐藏域
     * @param redirect_url  来源url
     * @param model     //
     * @param sso_token 存储在浏览器本地的cookie数据
     * @return
     */
    // TODO @CookieValue
    @GetMapping("/login.html")
    public String loginPage(@RequestParam("redirect_url") String redirect_url,
                            Model model,
                            @CookieValue(value = "sso_token", required = false) String sso_token) {
        if (!StringUtils.isEmpty(sso_token)) return "redirect:" + redirect_url + "?token=" + sso_token;

        model.addAttribute("redirect_url", redirect_url);
        return "login";
    }

    /**
     * 处理用户的登录请求、
     * 如果登录成功，则存储用户数据于session
     * @param username
     * @param redirect_url
     * @return
     */
    @PostMapping(value = "/login")
    public String doLogin(@RequestParam("username") String username,
                          @RequestParam("password") String password,
                          @RequestParam("redirect_url") String redirect_url,
                          HttpServletResponse response) {


        if (!StringUtils.isEmpty(username) && !StringUtils.isEmpty(password)) {
            //登录成功跳转，跳回到登录页

            //把登录成功的用户存起来
            String uuid = UUID.randomUUID().toString().replace("_", "");
            redisTemplate.opsForValue().set(uuid, username);
            Cookie sso_token = new Cookie("sso_token", uuid);

            response.addCookie(sso_token); // 请求头会加上 Set-Cookie: sso_token=${uuid}
            return "redirect:" + redirect_url + "?token=" + uuid; // 请求头会加上 Location: url?token=${uuid}
        }
        return "login";
    }

}
