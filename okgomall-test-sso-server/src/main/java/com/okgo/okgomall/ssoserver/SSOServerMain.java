package com.okgo.okgomall.ssoserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SSOServerMain {

    public static void main(String[] args) {
        SpringApplication.run(SSOServerMain.class, args);
    }

}
