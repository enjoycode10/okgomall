package com.okgo.common.to.mq;

import lombok.Data;

/**
 * @author Shawn
 * @date 2020/10/8 23:13
 * @title 发送到mq消息队列的to
 */
@Data
public class StockLockedTo {

    /** 库存工作单的id **/
    private Long id;

    /** 工作单详情的所有信息 **/
    private StockDetailTo detailTo;
}
