package com.okgo.common.to;

import lombok.Data;

/**
 * @author Shawn
 * @date 2020/9/16 22:56
 * @title Function
 */
@Data
public class SkuHasStockVo {
    private Long skuId;
    private Boolean hasStock;
}
