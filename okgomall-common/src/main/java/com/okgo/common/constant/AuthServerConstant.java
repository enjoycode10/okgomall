package com.okgo.common.constant;

/**
 * @author Shawn
 * @date 2020/9/27 10:47
 * @title Function
 */
public class AuthServerConstant {

    public static final String SMS_CODE_CACHE_PREFIX = "sms:code:";

    public static final String LOGIN_USER = "loginUser";


}
