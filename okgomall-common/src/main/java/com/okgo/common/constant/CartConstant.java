package com.okgo.common.constant;

/**
 * @author Shawn
 * @date 2020/9/29 20:47
 * @title Function
 */
public class CartConstant {

    public final static String TEMP_USER_COOKIE_NAME = "user-key"; // 临时用户key

    public final static int TEMP_USER_COOKIE_TIMEOUT = 60*60*24*30; // 过期时间1个月

    public final static String CART_PREFIX = "okgomall:cart:";


}
