package com.okgo.okgomall.member.vo;

import lombok.Data;

/**
 * @author Shawn
 * @date 2020/9/27 14:15
 * @title 社交用户信息
 */

@Data
public class SocialUser {

    private String access_token;

    private String remind_in;

    private long expires_in;

    private String uid;

    private String isRealName;

}
