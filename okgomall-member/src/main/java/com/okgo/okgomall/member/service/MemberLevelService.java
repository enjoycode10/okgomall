package com.okgo.okgomall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.okgo.common.utils.PageUtils;
import com.okgo.okgomall.member.entity.MemberLevelEntity;

import java.util.Map;

/**
 * 会员等级
 *
 * @author Shawn
 * @email enjoycode10@163.com
 * @date 2020-07-17 08:49:58
 */
public interface MemberLevelService extends IService<MemberLevelEntity> {

    com.okgo.common.utils.PageUtils queryPage(Map<String, Object> params);
}

