package com.okgo.okgomall.member.vo;

import lombok.Data;

/**
 * @author Shawn
 * @date 2020/9/27 14:15
 * @title Function
 */

@Data
public class MemberUserRegisterVo {

    private String userName;

    private String password;

    private String phone;

}
