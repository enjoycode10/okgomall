package com.okgo.okgomall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.okgo.common.utils.PageUtils;
import com.okgo.okgomall.member.entity.MemberLoginLogEntity;

import java.util.Map;

/**
 * 会员登录记录
 *
 * @author Shawn
 * @email enjoycode10@163.com
 * @date 2020-07-17 08:49:58
 */
public interface MemberLoginLogService extends IService<MemberLoginLogEntity> {

    com.okgo.common.utils.PageUtils queryPage(Map<String, Object> params);
}

