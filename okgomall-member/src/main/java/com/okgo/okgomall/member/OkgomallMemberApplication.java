package com.okgo.okgomall.member;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@EnableRedisHttpSession
@EnableFeignClients("com.okgo.okgomall.member.feign")
@EnableDiscoveryClient
@MapperScan("com.okgo.okgomall.member.dao")
@SpringBootApplication
public class OkgomallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(OkgomallMemberApplication.class, args);
    }

}
