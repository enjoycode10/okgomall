package com.okgo.okgomall.member.exception;

/**
 * @author Shawn
 * @date 2020/9/27 14:15
 * @title 用户信息异常
 */
public class UsernameException extends RuntimeException {


    public UsernameException() {
        super("存在相同的用户名");
    }
}
