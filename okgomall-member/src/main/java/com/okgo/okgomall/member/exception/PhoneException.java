package com.okgo.okgomall.member.exception;

/**
 * @author Shawn
 * @date 2020/9/27 14:15
 * @title 手机号信息异常
 */
public class PhoneException extends RuntimeException {

    public PhoneException() {
        super("存在相同的手机号");
    }
}
