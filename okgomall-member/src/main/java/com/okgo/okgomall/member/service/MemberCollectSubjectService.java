package com.okgo.okgomall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.okgo.common.utils.PageUtils;
import com.okgo.okgomall.member.entity.MemberCollectSubjectEntity;

import java.util.Map;

/**
 * 会员收藏的专题活动
 *
 * @author Shawn
 * @email enjoycode10@163.com
 * @date 2020-07-17 08:49:58
 */
public interface MemberCollectSubjectService extends IService<MemberCollectSubjectEntity> {

    com.okgo.common.utils.PageUtils queryPage(Map<String, Object> params);
}

