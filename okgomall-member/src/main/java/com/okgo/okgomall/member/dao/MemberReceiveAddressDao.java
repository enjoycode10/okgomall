package com.okgo.okgomall.member.dao;

import com.okgo.okgomall.member.entity.MemberReceiveAddressEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收货地址
 * 
 * @author Shawn
 * @email enjoycode10@163.com
 * @date 2020-07-17 08:49:58
 */
@Mapper
public interface MemberReceiveAddressDao extends BaseMapper<MemberReceiveAddressEntity> {
	
}
