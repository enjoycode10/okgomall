package com.okgo.okgomall.order.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author Shawn
 * @date 2020/10/1 14:07
 * @title Function
 */
@Data
public class FareVo {

    private MemberAddressVo address;

    private BigDecimal fare;

}
