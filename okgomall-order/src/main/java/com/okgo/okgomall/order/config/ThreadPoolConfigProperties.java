package com.okgo.okgomall.order.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Shawn
 * @date 2020/9/21 21:00
 * @title 线程池属性
 */

@ConfigurationProperties(prefix = "okgomall.thread")
@Component
@Data
public class ThreadPoolConfigProperties {

    private Integer coreSize;

    private Integer maxSize;

    private Integer keepAliveTime;


}
