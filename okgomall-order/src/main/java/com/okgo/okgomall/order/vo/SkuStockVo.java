package com.okgo.okgomall.order.vo;

import lombok.Data;

/**
 * @author Shawn
 * @date 2020/10/1 14:07
 * @title 库存vo
 */
@Data
public class SkuStockVo {

    private Long skuId;

    private Boolean hasStock;

}
