package com.okgo.okgomall.order.config;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.okgo.okgomall.order.vo.PayVo;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "alipay")
@Component
@Data
public class AlipayTemplate {

    //在支付宝创建的应用的id
    private String app_id = "2016102400752621";

    // 商户私钥，您的PKCS8格式RSA2私钥
    private String merchant_private_key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQClEVGVJXSRpzYdJrO97H1K+jteZDSZsEiQHJ+AptP6Pgnnp3iJSSOrCcwORHvTXGPtnRtzNhtNnasMRQqkikmIkDtHIb4b8CO1AUftk6ojKEjVQwSAYo3EIZpEuZf7wpM0nL8TVBiRpTn4gsgw46Y7IZ7PYuSZCaun6Nti5CA1WOrEFY+u+Z4cTtm3+3KOBSE++ps3znIYM6UmFnT2hLWK0Fat4dF2ElPR6/s+4VRkoHayk3l9T4OeEi6lgQf7VOqHv4zbXut8LnbcKlO3C/nId7NlA88OuSaxDw2lKuffUCO4Ojdkng5eKVbcvRLyau5DVlEi+2eGWfzJPR3pH5XDAgMBAAECggEAVwX9Y8seXJeyEMZRpoRiPcAUwpvDm27zon5knNfXE6vXXvjMtFwJ4OdGXBNA0Np5mFDmouPYmx/PiKVxifOZ9tRLXCHu7x24RXoKaLcb0HMxirVNTz4mDUvF4AuUk9ct8mPc3JzOYyKfoGOalSO/hJch+aXibMhLZjd+SfGJBXYk8rKOyHfldATGJolnBM26gKs/ZwRJJWSRPqfIogKmtIUrkgop/sMA1F4CZ5Tg23CZvRWk76Qt3QTSMUL6omZZBUxtzaBOmP1rwAJv1PcGfFpLs63U3BlUlhpwe4VmlrlidjgvaVtFu9A62SQPOL682fulJ4JFVpoYv74rIihpeQKBgQDcPdRf30bMyu+Y76YWIBSBj3BYkfLo9Qzx/p4L6h/O2F55UEFdAdxQO0ri5BpoKIDoyjWFTqGkj7gHjY55OI6LUjzb99GOg1BRXJ2eh6IDu91ywN6u12kdrJ4caQph9Nt7lFsNt5vvHMvem5u5gngtXOcQz5lOdpB/DYbf2ApzHQKBgQC/3jq3AHvkj39oiAYR0sSf9t08qqV0KA0FkQu4fEXlK2GC+kjLdlwWEmsTSiX9Hk4AjBplNaw5Aug12jJh6MjwmP/wCcp9ETMrqNzbEPC7zx00aJecZaYzY1ysZwVDaoa6Dw+/oR5fi6bCDHU+Gw9N/4qIDAVN81EyrYfxQlX2XwKBgQDH1FXIcyajFBJW+kIA2zSf5UyzICF9WzJWs4YlG1Wm8MyvV7EmuVRze4jT2tFW5kEodqge/xxM2pXITJkTxcwX/xL3JqB9pQmP+O+OclP+/UrNHtAtZYebhA6lim0AOhGWzCHdjVWvm7oijZ+6rvVpe1yv9RyNg6hRjy1sFnJMxQKBgQCLGbklnZM0pBgxl5QW7eAhPN8uMWVcbgDqzTpbGLWvrKC4TW3jHD8svVOlm3cpKARS/z/Pqnbeqk3hZPxr4VF0pOfAhcionYSknXro00JEVqaMrVtlRoiU7u9sChBVzPA6CdtYCZ40lNFkE5YYXZPd72rmsRhTaBixDf/lmDssLwKBgCHBeuWivQKfiJECPbYs3029G2Fsvx4uBAhfe9AY8z9s0GvC5QHHQ7pL2MbeP3BfDeNjrfIR0SpYkn53mre73O278lxA6wOwqbQkF5bRwujBS9+Tg/tZTYromNm+JZVJRGv9796RWJmM3YiUH4mZ1SnmPrPtyG//nF/yLY3zB4+i";
    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    private String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApRMbpOJBZMnX8idKC+CwGqoQZLpTAWf88ymUs1JVWJKkeFF8lYDF4lTYzl1qUazivaocm76RYk5PMG443yRhFe9PTv4EpR973wpZlphro3wafvsGDclcHNqphRgofcIH/4G/JK7Rk6/RwXtG1pR8TDYXyv2mbFCQu/6eR5086ha4VvRgNTy2+fCDq/0Q27P/RyV9eIBfd9qF3OzymINuVqDaR+okicJOs3VaAOmD8kNmGK3rwrOs11DicBJQPjHc+Hea0ntpXYkKqD8L8e8LG20NikdOFdV5MRWyVtpPCwZVt2jHbkcc894SQ52AZFE3Gkzjr5GKwuNhCjZnpOg4SQIDAQAB";
    // 服务器[异步通知]页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    // 支付宝会悄悄的给我们发送一个请求，告诉我们支付成功的信息
    private String notify_url = "http://9unzih3w4j.52http.tech/paid/notify";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    //同步通知，支付成功，一般跳转到成功页
    private String return_url = "http://memeber.okgomall.com/memeberOrder.html";

    // 签名方式
    private String sign_type = "RSA2";

    // 字符编码格式
    private String charset = "utf-8";

    // 超时
    private String timeout = "30m";

    // 支付宝网关； https://openapi.alipaydev.com/gateway.do
    private String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    public String pay(PayVo vo) throws AlipayApiException {

        //AlipayClient alipayClient = new DefaultAlipayClient(AlipayTemplate.gatewayUrl, AlipayTemplate.app_id, AlipayTemplate.merchant_private_key, "json", AlipayTemplate.charset, AlipayTemplate.alipay_public_key, AlipayTemplate.sign_type);
        //1、根据支付宝的配置生成一个支付客户端
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrl,
                app_id, merchant_private_key, "json",
                charset, alipay_public_key, sign_type);

        //2、创建一个支付请求 //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(return_url);
        alipayRequest.setNotifyUrl(notify_url);

        //商户订单号，商户网站订单系统中唯一订单号，必填
        String out_trade_no = vo.getOut_trade_no();
        //付款金额，必填
        String total_amount = vo.getTotal_amount();
        //订单名称，必填
        String subject = vo.getSubject();
        //商品描述，可空
        String body = vo.getBody();

        alipayRequest.setBizContent("{\"out_trade_no\":\"" + out_trade_no + "\","
                + "\"total_amount\":\"" + total_amount + "\","
                + "\"subject\":\"" + subject + "\","
                + "\"body\":\"" + body + "\","
                + "\"timeout_express\":\"" + timeout + "\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        String result = alipayClient.pageExecute(alipayRequest).getBody();

        //会收到支付宝的响应，响应的是一个页面，只要浏览器显示这个页面，就会自动来到支付宝的收银台页面
        System.out.println("支付宝的响应：" + result);

        return result;

    }
}
