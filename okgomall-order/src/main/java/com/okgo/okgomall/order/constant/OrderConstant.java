package com.okgo.okgomall.order.constant;

/**
 * @author Shawn
 * @date 2020/10/1 17:25
 * @title Function
 */
public class OrderConstant {

    public static final String USER_ORDER_TOKEN_PREFIX = "order:token";

}
