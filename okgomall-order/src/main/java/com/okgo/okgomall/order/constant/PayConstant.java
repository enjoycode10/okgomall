package com.okgo.okgomall.order.constant;

/**
 * @author Shawn
 * @date 2020/10/1 17:30
 * @title Function
 */
public class PayConstant {

    public static final Integer ALIPAY = 1;

    public static final Integer WXPAY = 2;

}
