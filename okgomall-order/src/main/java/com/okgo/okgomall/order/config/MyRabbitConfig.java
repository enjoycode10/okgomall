package com.okgo.okgomall.order.config;

import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.annotation.PostConstruct;

/**
 * @author Shawn
 * @date 2020-09-30 23:22
 * @title Function
 */
@Configuration
public class MyRabbitConfig {

    //@Autowired
    private RabbitTemplate rabbitTemplate;

    //public MyRabbitConfig(RabbitTemplate rabbitTemplate){
    //    this.rabbitTemplate = rabbitTemplate;
    //    initRabbitTemplate();
    //}


    @Primary
    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        this.rabbitTemplate = rabbitTemplate;
        rabbitTemplate.setMessageConverter(messageConverter());
        initRabbitTemplate();
        return rabbitTemplate;
    }

    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    /**
     * 定制RabbitTemplate
     * 1、消息代理收到消息就会回调
     *      1、spring.rabbitmq.publisher-confirms: true
     *      2、设置确认回调 setConfirmCallback
     *
     * 2、消息正确抵达队列就会进行回调
     *      1、spring.rabbitmq.publisher-returns: true
     *         spring.rabbitmq.template.mandatory: true
     *      2、设置确认回调 setReturnCallback
     *
     * 3、消费端确认(保证每个消息都被正确消费，此时才可以让broker中的queue删除这个消息)
     *      1、默认是自动确认的，只要消息被consumer消费，会自动触发确认，broker中的queue删除这个消息
     *          问题：queue有很多消息，consumer收到后自动回复ack，只有一个消息处理成功，然后consumer宕机，其他消息丢失
     *          解决：手动确认(自动模式只要抵达consumer就确认，有可能未被正常消费)
     *              spring.rabbitmq.listener.simple.acknowledge-mode=manual
     *              channel.basicAck()：签收，业务成功就应该签收
     *              channel.basicNack()：拒签，业务失败拒签
     */
    //@PostConstruct  //MyRabbitConfig对象创建完成以后，执行这个方法
    public void initRabbitTemplate() {

        /**
         * 1、只要消息抵达Broker就ack=true
         * correlationData：当前消息的唯一关联数据(这个是消息的唯一id)
         * ack：消息是否成功收到
         * cause：失败的原因
         */
        //设置确认回调
        rabbitTemplate.setConfirmCallback((correlationData,ack,cause) -> {
            // 1. 做好消息的确认机制(publicsher, consumer 【手动确认ack】)
            // 2. 每一个发送的消息都在数据库做好记录，定期将失败的消息再次发送
            System.out.println("confirm...correlationData["+correlationData+"]==>ack:["+ack+"]==>cause:["+cause+"]");
        });


        /**
         * 只要消息没有投递给指定的队列，就触发这个失败回调；投递成功，则不触发
         * message：投递失败的消息详细信息
         * replyCode：回复的状态码
         * replyText：回复的文本内容
         * exchange：当时这个消息发给哪个交换机
         * routingKey：当时这个消息用哪个路邮键
         */
        rabbitTemplate.setReturnCallback((message,replyCode,replyText,exchange,routingKey) -> {
            // 如果报错，修改数据库中当前消息的状态
            System.out.println("==>Fail Message["+message+"]\n==>replyCode["+replyCode+"]" +
                    "\n==>replyText["+replyText+"]\n==>exchange["+exchange+"]\n==>routingKey["+routingKey+"]");
        });
    }
}
