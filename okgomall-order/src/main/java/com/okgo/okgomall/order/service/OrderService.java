package com.okgo.okgomall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.okgo.common.to.mq.SeckillOrderTo;
import com.okgo.common.utils.PageUtils;
import com.okgo.okgomall.order.entity.OrderEntity;
import com.okgo.okgomall.order.vo.*;

import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * 订单
 *
 * @author Shawn
 * @email enjoycode10@163.com
 * @date 2020-07-17 08:59:08
 */
public interface OrderService extends IService<OrderEntity> {

    com.okgo.common.utils.PageUtils queryPage(Map<String, Object> params);

    OrderConfirmVo confirmOrder() throws ExecutionException, InterruptedException;

    SubmitOrderResponseVo submitOrder(OrderSubmitVo vo);

    OrderEntity getOrderByOrderSn(String orderSn);

    void closeOrder(OrderEntity orderEntity);

    PayVo getOrderPay(String orderSn);

    PageUtils queryPageWithItem(Map<String, Object> params);

    String handlePayResult(PayAsyncVo asyncVo);

    void createSeckillOrder(SeckillOrderTo orderTo);
}

