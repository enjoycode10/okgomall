package com.okgo.okgomall.order.vo;

import com.okgo.okgomall.order.entity.OrderEntity;
import lombok.Data;


/**
 * @author Shawn
 * @date 2020/10/1 14:07
 * @title Function
 */
@Data
public class SubmitOrderResponseVo {

    /** 下单成功：订单信息 **/
    private OrderEntity order;

    /** 下单失败：错误状态码，0成功 **/
    private Integer code;


}
