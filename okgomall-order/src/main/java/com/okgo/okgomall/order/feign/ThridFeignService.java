package com.okgo.okgomall.order.feign;

import com.okgo.okgomall.order.vo.PayVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author Shawn
 * @date 2020/10/1 14:30
 * @title Function
 */
@FeignClient("okgomall-tps")
public interface ThridFeignService {

    @GetMapping(value = "/pay",consumes = "application/json")
    String pay(@RequestBody PayVo vo) throws AlipayApiException;

}
