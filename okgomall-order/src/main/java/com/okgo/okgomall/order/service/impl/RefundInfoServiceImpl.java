package com.okgo.okgomall.order.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.okgo.common.utils.PageUtils;
import com.okgo.common.utils.Query;

import com.okgo.okgomall.order.dao.RefundInfoDao;
import com.okgo.okgomall.order.entity.RefundInfoEntity;
import com.okgo.okgomall.order.service.RefundInfoService;


@Service("refundInfoService")
public class RefundInfoServiceImpl extends ServiceImpl<RefundInfoDao, RefundInfoEntity> implements RefundInfoService {

    @Override
    public com.okgo.common.utils.PageUtils queryPage(Map<String, Object> params) {
        IPage<RefundInfoEntity> page = this.page(
                new com.okgo.common.utils.Query<RefundInfoEntity>().getPage(params),
                new QueryWrapper<RefundInfoEntity>()
        );

        return new com.okgo.common.utils.PageUtils(page);
    }

}