package com.okgo.okgomall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.okgo.common.utils.PageUtils;
import com.okgo.okgomall.order.entity.OrderItemEntity;

import java.util.Map;

/**
 * 订单项信息
 *
 * @author Shawn
 * @email enjoycode10@163.com
 * @date 2020-07-17 08:59:08
 */
public interface OrderItemService extends IService<OrderItemEntity> {

    com.okgo.common.utils.PageUtils queryPage(Map<String, Object> params);
}

