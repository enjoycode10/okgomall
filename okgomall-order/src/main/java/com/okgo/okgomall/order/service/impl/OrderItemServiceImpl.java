package com.okgo.okgomall.order.service.impl;

import com.okgo.okgomall.order.entity.OrderEntity;
import com.okgo.okgomall.order.entity.OrderReturnReasonEntity;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.okgo.common.utils.PageUtils;
import com.okgo.common.utils.Query;

import com.okgo.okgomall.order.dao.OrderItemDao;
import com.okgo.okgomall.order.entity.OrderItemEntity;
import com.okgo.okgomall.order.service.OrderItemService;


@Service("orderItemService")
@RabbitListener(queues = {"hello-java-queue"})
public class OrderItemServiceImpl extends ServiceImpl<OrderItemDao, OrderItemEntity> implements OrderItemService {

    @Override
    public com.okgo.common.utils.PageUtils queryPage(Map<String, Object> params) {
        IPage<OrderItemEntity> page = this.page(
                new com.okgo.common.utils.Query<OrderItemEntity>().getPage(params),
                new QueryWrapper<OrderItemEntity>()
        );

        return new com.okgo.common.utils.PageUtils(page);
    }

    /**
     * queues：声明需要监听的队列，队列可以有很多client来监听，但只能有一个能消费消息，消费后删除。
     * 场景：
     *      1. 订单服务启动多个：同一个消息，只能有一个服务消费掉
     *      2. 只有一个消息完全处理完，方法运行结束，才可以接受到下一个消息
     *
     * 方法的输入参数可以写一下消息类型：
     *      1. Message message: 原生消息，包括消息头和消息体
     *      2. T<发送的消息类型>: OrderReturnReasonEntity content
     *      3. Channel channel: 当前传输数据的通道
     */
    @RabbitHandler
    public void revieveMessage(Message message,
                               OrderReturnReasonEntity content, Channel channel) {
        // 拿到主体内容
        byte[] body = message.getBody();
        // 拿到的消息头属性信息
        MessageProperties messageProperties = message.getMessageProperties();
        System.out.println("接受到的消息...内容" + message + "===内容：" + content);
        // 通道channel内按顺序自增的
        long deliveryTag = message.getMessageProperties().getDeliveryTag();

        try {
            // 非批量签收模式
            if (deliveryTag%2==0) channel.basicAck(deliveryTag, false);
            System.out.println("签收了..."+deliveryTag);
            // basicNack(long deliveryTag, boolean multiple, boolean requeue)
            // basicReject(long deliveryTag, boolean requeue)
            if (deliveryTag%2!=0) channel.basicNack(deliveryTag,false,false); // requeue：true重新发送到队列，然后又发送给consumer继续判断是否消费; false直接丢弃
            if (deliveryTag%2!=0) channel.basicReject(deliveryTag,false); // 比basicReject() 多multiple批量处理
        } catch (IOException e) {
            // 网络中断/退货模式
            System.out.println("未签收..."+deliveryTag);
            e.printStackTrace();
        }

    }

    @RabbitHandler
    public void revieveMessage(Message message,
                               OrderEntity content, Channel channel) {
        //拿到主体内容
        byte[] body = message.getBody();
        //拿到的消息头属性信息
        MessageProperties messageProperties = message.getMessageProperties();
        System.out.println("接受到的消息...内容" + message + "===内容：" + content);

    }
}