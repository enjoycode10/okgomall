package com.okgo.okgomall.order.to;

import com.okgo.okgomall.order.entity.OrderEntity;
import com.okgo.okgomall.order.entity.OrderItemEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Shawn
 * @date 2020/10/1 14:07
 * @title Function
 */
@Data
public class OrderCreateTo {

    private OrderEntity order;

    private List<OrderItemEntity> orderItems;

    /** 订单计算的应付价格 **/
    private BigDecimal payPrice;

    /** 运费 **/
    private BigDecimal fare;

}
