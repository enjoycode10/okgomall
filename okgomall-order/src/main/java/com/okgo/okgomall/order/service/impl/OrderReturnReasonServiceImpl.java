package com.okgo.okgomall.order.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.okgo.common.utils.PageUtils;
import com.okgo.common.utils.Query;

import com.okgo.okgomall.order.dao.OrderReturnReasonDao;
import com.okgo.okgomall.order.entity.OrderReturnReasonEntity;
import com.okgo.okgomall.order.service.OrderReturnReasonService;


@Service("orderReturnReasonService")
public class OrderReturnReasonServiceImpl extends ServiceImpl<OrderReturnReasonDao, OrderReturnReasonEntity> implements OrderReturnReasonService {

    @Override
    public com.okgo.common.utils.PageUtils queryPage(Map<String, Object> params) {
        IPage<OrderReturnReasonEntity> page = this.page(
                new com.okgo.common.utils.Query<OrderReturnReasonEntity>().getPage(params),
                new QueryWrapper<OrderReturnReasonEntity>()
        );

        return new com.okgo.common.utils.PageUtils(page);
    }

}