package com.okgo.okgomall.order;

import com.okgo.okgomall.order.entity.OrderEntity;
import com.okgo.okgomall.order.entity.OrderReturnReasonEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

//@Runwith(SpringRunner.class)
@Slf4j
@SpringBootTest
class OkgomallOrderApplicationTests {


    /**
     * 1. 使用AmqpAdmin进行创建
     *      exchange(hello-java-exchange)
     *      queue
     *      binding
     *
     *
     * 2. 如何收发消息
     *
     */

    @Autowired
    AmqpAdmin amqpAdmin;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    void contextLoads() {
    }

    @Test
    public void createExchagne(){
        // DirectExchange(String name, boolean durable, boolean autoDelete, Map<String, Object> arguments)
        amqpAdmin.declareExchange(new DirectExchange("hello-java-exchange", true, false));
        log.info("Exchange[{}]创建成功", "hello-java-exchange");
    }

    @Test
    public void testCreateQueue() {
        // Queue(String name, boolean durable, boolean exclusive, boolean autoDelete, @Nullable Map<String, Object> arguments)
        // exclusive: 排他，如果有连接已经连接这个queue，其他连接则不能再连接这个queue，一般不设置为排他
        Queue queue = new Queue("hello-java-queue",true,false,false);
        amqpAdmin.declareQueue(queue);
        log.info("Queue[{}]创建成功：","hello-java-queue");
    }


    @Test
    public void createBinding() {
        // Binding( String destination, 目的地可以是queue或exchange的名称
        //          Binding.DestinationType destinationType, 目的地可以是queue或exchange
        //          String exchange, 当前的交换机exchange
        //          String routingKey, 路由键
        //          @Nullable Map<String, Object> arguments) 自定义参数
        Binding binding = new Binding("hello-java-queue",
                Binding.DestinationType.QUEUE,
                "hello-java-exchange",
                "hello.java",
                null);
        amqpAdmin.declareBinding(binding);
        log.info("Binding[{}]创建成功：","hello-java-binding");

    }

    @Test
    public void create() {
        HashMap<String, Object> arguments = new HashMap<>();
        arguments.put("x-dead-letter-exchange", "order-event-exchange");
        arguments.put("x-dead-letter-routing-key", "order.release.order");
        arguments.put("x-message-ttl", 60000); // 消息过期时间 1分钟
        Queue queue = new Queue("order.delay.queue", true, false, false, arguments);
        amqpAdmin.declareQueue(queue);
        log.info("Queue[{}]创建成功：","order.delay.queue");
    }

    @Test
    public void sendMessageTest() {
        //1、发送消息，如果发送的消息是个对象，会使用序列化机制，将对象写出去，对象必须实现Serializable接口
        String msg = "Hello World";

        //2、发送的对象类型的消息，可以是一个json，
        for (int i = 0; i < 10; i++) {
            if (i%2==0){
                // 退货原因实体类
                OrderReturnReasonEntity reasonEntity = new OrderReturnReasonEntity();
                reasonEntity.setId(1L);
                reasonEntity.setCreateTime(new Date());
                reasonEntity.setName("haha--O(∩_∩)O哈哈~=="+i);
                reasonEntity.setStatus(1);
                reasonEntity.setSort(2);
                rabbitTemplate.convertAndSend("hello-java-exchange","hello.java",
                        reasonEntity,new CorrelationData(UUID.randomUUID().toString()));
            }else {
                // 退货原因实体类
                OrderEntity orderEntity = new OrderEntity();
                orderEntity.setOrderSn(UUID.randomUUID().toString());
                rabbitTemplate.convertAndSend("hello-java-exchange","hello.java",
                        orderEntity,new CorrelationData(UUID.randomUUID().toString()));
            }
        }
        log.info("消息发送完成:{}");
    }


}
