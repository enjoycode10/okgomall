package com.okgo.okgomall.search;

import com.alibaba.fastjson.JSON;
import com.okgo.okgomall.search.config.OkgomallElasticSearchConfig;
import lombok.Data;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

/**
 * @author Shawn
 * @date 2020/9/14 9:27
 * @title Function
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ESTest {

    @Autowired
    private RestHighLevelClient client;

    @Test
    public void contextLoads() {
        System.out.println(client);
    }

    /**
     * 存储数据到ES -- 方式1：放入json
     */
    @Test
    public void indexData1() {
        IndexRequest request = new IndexRequest("users");   // Index
        request.id("1");    // Document id for the request
        String jsonString = "{" +
                "\"user\":\"kimchy\"," +
                "\"postDate\":\"2013-01-30\"," +
                "\"message\":\"trying out Elasticsearch\"" +
                "}";
        request.source(jsonString, XContentType.JSON);  // Document source provided as a String
    }

    /**
     * 存储数据到ES -- 方式2：放入object的json string
     */
    @Test
    public void indexData2() throws IOException {
        IndexRequest request = new IndexRequest("users");   // Index
        request.id("2");    // Document id for the request，不设置会自动生成
        User user = new User();
        String jsonString = JSON.toJSONString(user);
        request.source(jsonString, XContentType.JSON);
        IndexResponse indexResponse = client.index(request, RequestOptions.DEFAULT);
        System.out.println(indexResponse);
    }

    /**
     * 存储数据到ES -- 方式3：放入object的key-pairs
     */
    @Test
    public void indexData3() {
        IndexRequest request = new IndexRequest("users");   // Index
        request.id("3");    // Document id for the request，不设置会自动生成
        request.source("user", "kimchy",
                "postDate", new Date(),
                "message", "trying out Elasticsearch");  // Document source provided as Object key-pairs, which gets converted to JSON format
    }


    @Test
    public void searchData() throws IOException {
        // 1. 创建检所请求
        SearchRequest searchRequest = new SearchRequest();
        // 指定索引
        searchRequest.indices("bank");
        // 指定DSL, 检索条件
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        // 1.1 构造检索条件
        //searchSourceBuilder.query();
        //searchSourceBuilder.from();
        //searchSourceBuilder.size();
        //searchSourceBuilder.aggregation();
        searchSourceBuilder.query(QueryBuilders.matchQuery("address", "mill"));
        // 1.2 按照年龄的值分布进行聚合
        TermsAggregationBuilder aggregation = AggregationBuilders.terms("ageAgg").field("age").size(10);
        searchSourceBuilder.aggregation(aggregation);


        searchRequest.source(searchSourceBuilder);
        // 2. 执行检索
        SearchResponse searchResponse = client.search(searchRequest, OkgomallElasticSearchConfig.COMMON_OPTIONS);

        // 3. 分析结果
        System.out.println(searchResponse.toString());
        //JSON.parseObject(searchResponse.toString(), Map.class);
        // 3.1 获取所有查到的数据
        SearchHits hits = searchResponse.getHits();
        SearchHit[] searchHits = hits.getHits();
        for (SearchHit searchHit : searchHits) {
            // do something with the SearchHit
            searchHit.getIndex();
            searchHit.getType();
            String s = searchHit.getSourceAsString();
        }
        // 3.2 获取检索到的分析信息
        Aggregations aggregations = searchResponse.getAggregations();
        Terms ageAgg = aggregations.get("ageAgg");
        for (Terms.Bucket bucket : ageAgg.getBuckets()) {
            String keyAsString = bucket.getKeyAsString();
            System.out.println("年龄："+keyAsString+"==>"+bucket.getDocCount());
        }

    }

    @Data
    class User {
        private String userName;
        private String gender;
        private Integer age;
    }
}
