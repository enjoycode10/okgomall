package com.okgo.okgomall.search;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author Shawn
 * @date 2020/9/14 9:20
 * @title Function
 */
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class OkgomallSearchApplication {
    public static void main(String[] args) {
        SpringApplication.run(OkgomallSearchApplication.class, args);
    }
}
