package com.okgo.okgomall.search.service;

import com.okgo.common.to.es.SkuESModel;

import java.io.IOException;
import java.util.List;

/**
 * @author Shawn
 * @date 2020/9/17 8:39
 * @title Function
 */
public interface ProductSaveService {
    public boolean productStatusUp(List<SkuESModel> skuESModels) throws IOException;
}
