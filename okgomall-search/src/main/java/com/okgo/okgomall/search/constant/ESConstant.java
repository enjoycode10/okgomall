package com.okgo.okgomall.search.constant;

/**
 * @author Shawn
 * @date 2020/9/17 8:44
 * @title Function
 */
public class ESConstant {
    public static final String PRODUCT_INDEX = "product"; // sku数据在ES中的索引
    public static final Integer PRODUCT_PAGESIZE = 16;
}
