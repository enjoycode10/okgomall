package com.okgo.okgomall.search.service;

import com.okgo.okgomall.search.vo.SearchParam;
import com.okgo.okgomall.search.vo.SearchResult;


/**
 * @author Shawn
 * @date 2020/9/23 8:58
 * @title Function
 */
public interface MallSearchService {

    /**
     * @param param 检索的所有参数
     * @return  返回检索的结果，里面包含页面需要的所有信息
     */
    SearchResult search(SearchParam param);
}
