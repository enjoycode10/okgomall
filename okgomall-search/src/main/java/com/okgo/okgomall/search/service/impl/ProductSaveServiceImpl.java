package com.okgo.okgomall.search.service.impl;

import com.alibaba.fastjson.JSON;
import com.okgo.common.to.es.SkuESModel;
import com.okgo.okgomall.search.constant.ESConstant;
import com.okgo.okgomall.search.config.OkgomallElasticSearchConfig;
import com.okgo.okgomall.search.service.ProductSaveService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Shawn
 * @date 2020/9/17 8:41
 * @title Function
 */
@Slf4j
@Service
public class ProductSaveServiceImpl implements ProductSaveService {

    @Autowired
    RestHighLevelClient restHighLevelClient;
    @Override
    public boolean productStatusUp(List<SkuESModel> skuESModels) throws IOException {
        // 1. 给ES建立索引，建立映射关系

        // 2. 在ES中保存数据
        BulkRequest bulkRequest = new BulkRequest();
        for (SkuESModel model : skuESModels) {
            IndexRequest indexRequest = new IndexRequest(ESConstant.PRODUCT_INDEX);
            indexRequest.id(model.getSkuId().toString());
            String s = JSON.toJSONString(model);
            indexRequest.source(s, XContentType.JSON);

            bulkRequest.add(indexRequest);
        }
        BulkResponse bulk = restHighLevelClient.bulk(bulkRequest, OkgomallElasticSearchConfig.COMMON_OPTIONS);
        boolean b = bulk.hasFailures();
        List<String> collect = Arrays.stream(bulk.getItems()).map(BulkItemResponse::getId).collect(Collectors.toList());
        log.error("商品上架完成：{}", collect, bulk.toString());

        return b;
    }
}
