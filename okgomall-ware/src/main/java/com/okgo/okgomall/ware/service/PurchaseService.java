package com.okgo.okgomall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.okgo.common.utils.PageUtils;
import com.okgo.okgomall.ware.entity.PurchaseEntity;

import java.util.Map;

/**
 * 采购信息
 *
 * @author Shawn
 * @email enjoycode10@163.com
 * @date 2020-07-17 09:07:37
 */
public interface PurchaseService extends IService<PurchaseEntity> {

    com.okgo.common.utils.PageUtils queryPage(Map<String, Object> params);
}

