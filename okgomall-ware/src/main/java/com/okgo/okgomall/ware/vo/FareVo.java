package com.okgo.okgomall.ware.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author Shawn
 * @date 2020/10/1 17:41
 * @title Function
 */
@Data
public class FareVo {

    private MemberAddressVo address;

    private BigDecimal fare;

}


