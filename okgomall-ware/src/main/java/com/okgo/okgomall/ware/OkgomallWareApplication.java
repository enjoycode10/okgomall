package com.okgo.okgomall.ware;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableRabbit
@EnableDiscoveryClient
@MapperScan("com.okgo.okgomall.ware.dao")
@SpringBootApplication
public class OkgomallWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(OkgomallWareApplication.class, args);
    }

}
