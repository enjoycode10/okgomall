package com.okgo.okgomall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @author Shawn
 * @date 2020/10/1 14:07
 * @title 锁定库存的vo
 */
@Data
public class WareSkuLockVo {

    /** 为哪个订单号锁定库存 **/
    private String orderSn;

    /** 需要锁住的所有库存信息 **/
    private List<OrderItemVo> locks;



}
