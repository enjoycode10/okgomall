package com.okgo.okgomall.ware.vo;

import lombok.Data;

/**
 * @author Shawn
 * @date 2020/10/2 9:27
 * @title Function
 */

@Data
public class SkuHasStockVo {

    private Long skuId;

    private Boolean hasStock;

}
