package com.okgo.okgomall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.okgo.common.utils.PageUtils;
import com.okgo.okgomall.ware.entity.WareOrderTaskDetailEntity;

import java.util.Map;

/**
 * 库存工作单
 *
 * @author Shawn
 * @email enjoycode10@163.com
 * @date 2020-07-17 09:07:37
 */
public interface WareOrderTaskDetailService extends IService<WareOrderTaskDetailEntity> {

    com.okgo.common.utils.PageUtils queryPage(Map<String, Object> params);
}

