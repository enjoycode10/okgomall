package com.okgo.okgomall.ware.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.okgo.common.utils.PageUtils;
import com.okgo.common.utils.Query;

import com.okgo.okgomall.ware.dao.PurchaseDetailDao;
import com.okgo.okgomall.ware.entity.PurchaseDetailEntity;
import com.okgo.okgomall.ware.service.PurchaseDetailService;


@Service("purchaseDetailService")
public class PurchaseDetailServiceImpl extends ServiceImpl<PurchaseDetailDao, PurchaseDetailEntity> implements PurchaseDetailService {

    @Override
    public com.okgo.common.utils.PageUtils queryPage(Map<String, Object> params) {
        IPage<PurchaseDetailEntity> page = this.page(
                new com.okgo.common.utils.Query<PurchaseDetailEntity>().getPage(params),
                new QueryWrapper<PurchaseDetailEntity>()
        );

        return new com.okgo.common.utils.PageUtils(page);
    }

}