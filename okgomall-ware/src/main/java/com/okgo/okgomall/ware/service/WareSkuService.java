package com.okgo.okgomall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.okgo.common.to.OrderTo;
import com.okgo.common.to.SkuHasStockVo;
import com.okgo.common.to.mq.StockLockedTo;
import com.okgo.okgomall.ware.entity.WareSkuEntity;
import com.okgo.okgomall.ware.vo.WareSkuLockVo;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 商品库存
 *
 * @author Shawn
 * @email enjoycode10@163.com
 * @date 2020-07-17 09:07:37
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    com.okgo.common.utils.PageUtils queryPage(Map<String, Object> params);

    List<SkuHasStockVo> getSkusHasStock(List<Long> skuIds);

    boolean orderLockStock(WareSkuLockVo vo);

    void unlockStock(StockLockedTo to);

    @Transactional(rollbackFor = Exception.class)
    void unlockStock(OrderTo orderTo);
}

