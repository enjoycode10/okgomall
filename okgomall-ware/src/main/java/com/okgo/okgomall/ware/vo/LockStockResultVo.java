package com.okgo.okgomall.ware.vo;

import lombok.Data;

/**
 * @author Shawn
 * @date 2020/10/2 9:27
 * @title 库存的锁定结果
 */

@Data
public class LockStockResultVo {

    private Long skuId;

    private Integer num;

    /** 是否锁定成功 **/
    private Boolean locked;

}
