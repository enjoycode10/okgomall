package com.okgo.okgomall.ware.dao;

import com.okgo.okgomall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author Shawn
 * @email enjoycode10@163.com
 * @date 2020-07-17 09:07:37
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
